<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('category_list', function ($user) {
            return $user->checkPermissionAccess('list_category');
        });
        Gate::define('add_category', function ($user) {
            return $user->checkPermissionAccess('add_category');
        });
        Gate::define('edit_category', function ($user) {
            return $user->checkPermissionAccess('edit_category');
        });
        Gate::define('delete_category', function ($user) {
            return $user->checkPermissionAccess('delete_category');
        });

        Gate::define('list_product', function ($user) {
            return $user->checkPermissionAccess('list_product');
        });
        Gate::define('add_product', function ($user) {
            return $user->checkPermissionAccess('add_product');
        });
        Gate::define('edit_product', function ($user) {
            return $user->checkPermissionAccess('edit_product');
        });
        Gate::define('delete_product', function ($user) {
            return $user->checkPermissionAccess('delete_product');
        });

        Gate::define('list_user', function ($user) {
            return $user->checkPermissionAccess('list_user');
        });
        Gate::define('add_user', function ($user) {
            return $user->checkPermissionAccess('add_product');
        });
        Gate::define('edit_user', function ($user) {
            return $user->checkPermissionAccess('edit_user');
        });
        Gate::define('delete_user', function ($user) {
            return $user->checkPermissionAccess('delete_user');
        });

        Gate::define('list_role', function ($user) {
            return $user->checkPermissionAccess('list_role');
        });
        Gate::define('add_role', function ($user) {
            return $user->checkPermissionAccess('add_role');
        });
        Gate::define('edit_role', function ($user) {
            return $user->checkPermissionAccess('edit_role');
        });
        Gate::define('delete_role', function ($user) {
            return $user->checkPermissionAccess('delete_role');
        });
    }
}
