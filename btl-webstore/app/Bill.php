<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Cart;

class Bill extends Model
{
    public function userBill(){
       return $this->belongsTo(User::class,'user_id');
    }
    public function cart(){
        return $this->belongsTo(Cart::class,'bill_id');
    }
    public function billCart(){
        return $this->hasMany(Cart::class,'bill_id');
    }
    public function bill_products(){
        return $this->hasMany(BillProduct::class,'bill_id');
    }
}
