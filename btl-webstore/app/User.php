<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
     use Notifiable;
      use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
       public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    protected $fillable = [
       'id','user','password','full_name', 'email', 'phone_number','address',
    ];
    public $timestamps = false;

    public function roles()
    {
       return $this->belongsToMany( Role::class, 'role_user', 'user_id','role_id');
    }
    public function roleChil()
    {
       return $this->belongsToMany( Role::class, 'role_user', 'user_id','role_id');
    }

    public function checkPermissionAccess($permissionCheck)
    {
      //lay dc cai quyen cua user dang login he thong
      $roles = auth()->user()->roles;
      foreach ($roles as $role) {
         $permissions = $role->permissions;
         if($permissions->contains('key_code',$permissionCheck)){
            return true;
         }
      }
      return false;

      //so sanh gia tri dua vao cua router hien tai xem co ton tai trong cac quyen ma minh lay dc hay k

    }

}
