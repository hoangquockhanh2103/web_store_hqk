<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Role;

class Permission extends Model
{
    public function permissionChil()
    {
        return $this->hasMany( Permission::class, 'parent_id');
    }
}
