<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Permission;
class Role extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public $timestamps = false;

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'permission_role','role_id','permission_id');
    }
    
}
