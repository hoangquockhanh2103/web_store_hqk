<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Category;
use App\Products;
use Laravel\Scout\Searchable;


class HomeController extends Controller
{
    public function index(){
        $product = Products::latest()->paginate(8);
        $categories = Category::where('parent_id',0)->get();
        $categories1 = Category::where('parent_id',0)->latest()->paginate(8);;
        return view('pages.home.index',compact('categories','product','categories1'));
    }

    public function search(Request $request)
    {
         $search = $request->input('query');
         $ds = Products::where('name','LIKE','%'.$search.'%')->with('category')->get();
        return view('pages.SanPham.sanpham1',compact('ds'));
    }
}
