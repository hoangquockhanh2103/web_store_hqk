<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Cart;
use App\Bill;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use PHPUnit\Exception;
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class UserController extends Controller
{
    private $user;
    private $role;
    private $bill;
    public function __construct(Bill $bill, User $user ,Role $role){
        $this->user = $user;
        $this->role = $role;
        $this->bill = $bill;
    }
    public function create(){
        $roles = $this->role->all();

        return view('admin.users.add',compact('roles'));
    }

    public function index(){
       $users = $this->user->latest()->paginate(10);

        return view('admin.users.index',compact('users'));
    }
    public function store(Request $request){
        try{
            DB::beginTransaction();
            $user = $this->user->create([
                'user'=>$request->user,
                'password'=>$request->password,
                'full_name'=>$request->full_name,
                'email'=>$request->email,
                'phone_number'=>$request->phone,
                'address'=>$request->address
            ]);

            $roleIds = $request->role_id;
            $user->roles()->attach($roleIds);
            DB::commit();  

            return redirect()->route('users.index');
        }catch(\Exception $e){
            DB::rollBack();
            Log::error('Message :'.$e->getMessage(). 'line: '.$e->getLine());
        }
    }
    public function edit($id)
    {
        $users = $this->user->find($id);
        $roless = $users->roles; 
        $rolelist = $this->role->get();
        //dd($rolelist);
        return view('admin.users.edit',compact('roless','users','rolelist'));
    }
    public function update(Request $request, $id)
    {
            $user = $this->user->find($id);
            if($request->password != null){
                $pass = Hash::make($request->password);
            }
            else $pass = $user->password;
            //dd($pass);
            $this->user->find($id)->update([
                'user'=>$request->user,
                'password'=>$pass,
                'full_name'=>$request->full_name,
                'email'=>$request->email,
                'phone_number'=>$request->phone,
                'address'=>$request->address,
                'remember_token'=>null
            ]);

            $user = $this->user->find($id);
            $user->roles()->sync( $request->role_id);

           
            return redirect()->route('users.index');        
    }
    public function delete($id)
    {
        try {
            $user = $this->user->find($id);
            $this->user->find($id)->delete();
            return response()->json([
                'code'=>200,
                'message'=>'success',
                'name'=>$user->user
            ],200);
            
        } catch (Exception $e) {
            log::error('message: '.$e->getMessage().'line : '.$e->getLine());
            return response()->json([
                'code'=>500,
                'message'=>'fail'
            ],500);
        }
    }
    public function detailUser(){
        $id = Auth::id();
        $user = User::where('id',$id)->get();
        $cart = Cart::where('user_id',$id)->get();
        $bills = $this->bill->get();
        return view('pages.Home.detailUser',compact('user','bills'));
    }
    public function detailBill($id)
    {
        // $idUser = Auth::id();
        // $user = User::where('id',$idUser)->get();
        // $bills = $this->bill->find($id);
        // $key_id = $bills->key_id;
        // $carts = Cart::where('key_id',$key_id)->get();

        $a = Bill::where([
            'id'=>$id,
            'user_id'=> Auth::id()
        ])->with('bill_products')->get();
        dd($a);
       
        return view('pages.Home.detailBill',compact('a'));
    }
    public function updateuser(Request $request, $id)
    {
        DB::table('users')->where('id',$id)->update([
            'full_name'=>$request->full_name,
            'email' =>$request->email,
            'phone_number'=>$request->phone_number,
            'address'=>$request->address
        ]);

        return redirect()->route('user.detailUser');  
    }
    public function changePass(Request $request, $id)
    {
        $idUser = Auth::id();
        $user = User::where('id',$idUser)->get();
        $bills = $this->bill->get();

        return view('pages.Home.changePassword',compact('user','bills'));
    }
    public function updateUserPass($id, Request $request)
    {   
        $notifile = "Sai mat khau";
        $pass = Auth::User()->password;
        //dd(Hash::make($request->password));
        //dd($users);
        if(Hash::check($request->password,$pass)){
           $user_id = Auth::User()->id;                       
            $obj_user = User::find($user_id);
            $obj_user->password = $request->password_new;
            $obj_user->save(); 
            return redirect()->route('user.detailUser');  
        }
        else{
            return redirect()->back()->with("error","Sai mật khẩu !!");
        }
        if($request->password_new != $request->password_comfirm){
            return redirect()->back()->with("error","Mật khẩu không trùng khớp!");
        }
    }
}
