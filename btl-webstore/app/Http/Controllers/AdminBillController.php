<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\BillProduct;
use App\Cart;
use App\User;
use App\Products;
use DB;

class AdminBillController extends Controller
{
    private $bill;
    private $cart;

    public function __construct(Cart $cart, Bill $bill){
        $this->bill = $bill;
        $this->cart = $cart;
    }

    public function index()
    {
        $bills = $this->bill->latest()->paginate(10);
       
        return view('admin.bill.index',compact('bills'));
    }
    public function detail($id)
    {   $a = "";
        $bills = $this->bill->find($id);
        $select = $bills->status;
        if($select == 1 ){
            $a = "Đang đợi xét duyệt";
        }
        elseif($select == 2 ){
            $a = "Đã xét duyệt";
        }
        elseif($select == 3){
            $a = "Đang giao hàng";
        }
        elseif($select == 4) {
            $a = "Giao hàng thành công";
        }
        $sp = BillProduct::where('bill_id',$id)->with('products')->get();
        //$a = Products::where('id','26')->get();
        //dd($sp);
        $user = User::where('id',$bills->user_id)->first();
        
        return view('admin.bill.detail',compact('bills','sp','a','user'));
    }
    public function update(Request $request, $id){
        // $bills = DB::table('bills')->where('id',$id)->update(['status'=>$request->status]);
        // $billUpdate = $this->bill->find($id);
        $a = $request->status;
        $bill = Bill::where('id',$id)->update(['status'=>$a]);
        
        return redirect()->route('bill.index');
    }
}
