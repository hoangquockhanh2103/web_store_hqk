<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;

class AdminCartController extends Controller
{
    private $cart;

    public function __construct(Cart $cart){
        $this->cart = $cart;
    }

    public function index(){
         $carts = $this->cart->latest()->paginate(5);
        return view('admin.cart.index',compact('carts'));
    }
}
