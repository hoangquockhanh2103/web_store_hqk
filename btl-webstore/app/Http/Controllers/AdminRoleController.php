<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;

class AdminRoleController extends Controller
{
    private $role;
    private $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }
    public function index()
    {
        $roles = $this->role->paginate(10);

        return view('admin.role.index',compact('roles'));
    }
    public function create()
    {
        $permissionParent = $this->permission->where('parent_id',0)->get();
        
        return view('admin.role.add',compact('permissionParent'));
    }
    public function store(Request $request)
    {
        $role = $this->role->create([
            'name'=>$request->name,
            'display_name'=>$request->display_name
        ]);
        
        $role->permissions()->attach($request->permission_id);

        return redirect()->route('roles.index');
    }

    public function edit($id)
    {
        $permissionParent = $this->permission->where('parent_id',0)->get();
        $roles = $this->role->find($id);
        $checked = $roles->permissions;
        return view('admin.role.edit',compact('permissionParent','roles','checked'));
    }
    public function update(Request $request, $id)
    {
       $this->role->find($id)->update([
            'name'=>$request->name,
            'display_name'=>$request->display_name
        ]);
        $role = $this->role->find($id);
        $role->permissions()->sync($request->permission_id);

        return redirect()->route('roles.index');
    }
    public function delete($id)
    {
        try {
            $role = $this->role->find($id);
            $this->role->find($id)->delete();
            return response()->json([
                'code'=>200,
                'message'=>'success',
                'name'=>$role->name
            ],200);
            
        } catch (Exception $e) {
            log::error('message: '.$e->getMessage().'line : '.$e->getLine());
            return response()->json([
                'code'=>500,
                'message'=>'fail'
            ],500);
        }
    }
}
