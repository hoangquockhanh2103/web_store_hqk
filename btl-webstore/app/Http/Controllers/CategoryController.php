<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Recusive;
use App\Products;

class CategoryController extends Controller
{
    public function __construct(Category $category,Products $product){
        $this->product = $product;
        $this->category = $category;
    }

    public function create(){
        $htmlOption = $this->getCategory($parentId='');
        return view('admin.category.add',compact('htmlOption'));
    }

    public function getCategory($parentId){
        $data = $this->category->all();
        $recusive = new Recusive($data);
        $htmlOption = $recusive->categoryRecusive($parentId);

        return $htmlOption;
    }

    public function index(){
        $categories = $this->category->latest()->paginate(10);

        return view('admin.category.index', compact('categories'));
    }

    public function store(Request $request){
       $this->category->create([
       'name'=>$request->name,
        'parent_id'=>$request->parent_id
        ]);

       return redirect()->route('categories.index');
    }


    public function edit($id){
        $category = $this->category->find($id);
         $htmlOption = $this->getCategory($id);


        return view('admin.category.edit',compact('category','htmlOption'));

    }

    public function delete($id){
        $this->category->find($id)->delete();
        return redirect()->route('categories.index');
    }

    public function update($id,Request $request)
    {
        $this->category->find($id)->update([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id
        ]);
        return redirect()->route('categories.index');
    }
    public function show($id)
    {
        $showProducts = [];
        $idProduct=[];
        $showProduct = $this->category->find($id);
        if($showProduct->parent_id === 0){
            $a = $this->category->where('parent_id',$id)->get();
            foreach($a as $b){
                $showProductss = Products::where('category_id',$b->id)->get();
                foreach($showProductss as $aa){
                    array_push($idProduct,$aa->id);
                }
            }
            foreach($idProduct as $aaa){
                $productss = $this->product->find($aaa);
                array_push($showProducts,$productss);
            }
        }
        else $showProducts = $showProduct->productChil;
        
        $categories = Category::where('parent_id',0)->get();
        return view('pages.DanhMuc.show',compact('categories','showProducts'));
    }
}
