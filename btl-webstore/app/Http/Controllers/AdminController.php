<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function loginAdmin()
    {
        if(Auth::check()){
            return redirect('admin');
        }

        return view('login');
    }

    public function postLoginAdmin(Request $request)
    {
        $remember = $request->has('remember_me') ? true : false;
       if(auth()->attempt([
        'user'=>$request->user,
        'password'=>$request->password
       ], $remember)){
        return  view('admin');
       }

    }
    public function logout(Request $request) {
          Auth::logout();
          return redirect()->to('admin-login');
    }
}
