<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Products_image;
use App\Cart;

class ProductController extends Controller
{

    private $product;
    public function __construct( Products $product, Products_image $products_image,Cart $cart)
    {
        $this->product = $product;
        $this->products_image = $products_image;
        $this->cart = $cart;
    }

    public function index()
    {
        $products = $this->product->latest()->paginate(16);
        return view('pages.SanPham.sanpham',compact('products'));
    }
    public function detail($id)
    {
        $products = $this->product->find($id);
        $productall = $this->product->latest()->paginate(6);
        return view('pages.SanPham.detail',compact('products','productall'));
    }
    public function addToCart(Request $request, $id)
    {
        $sp = Products::find($id);
        $cart = session()->get('cart');

        if(isset($cart[$id])) {
            $cart[$id]['SL'] =  $cart[$id]['SL'] + 1;
        } else {
            $cart[$id] = [
                'id'=>$sp->id,
                'image'=>$sp->image_path,
                "name" => $sp->name,
                "SL" => 1,
                "price" => $sp->price
            ];
        }
        session()->put('cart', $cart);

        return response()->json([
            'code'=>200,
            'message'=>'success'
        ],200);
    }


}
