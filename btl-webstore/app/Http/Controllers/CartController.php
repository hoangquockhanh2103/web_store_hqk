<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Cart;
use App\Products;

class CartController extends Controller
{
    public function index()
    {
        $priceCart =0;
        $cart = session()->get('cart');
        if($cart != null){
            foreach($cart as $a){
                $b = $a['price'] * $a['SL'];
               $priceCart= $priceCart+ $b;
            }
        }
        //dd($priceCart);
        return view('pages.GioHang.show_cart',compact('cart','priceCart'));
    }
    public function deleteCart(Request $request)
    {
        $priceCart =0;
        $cart = session()->get('cart');
        foreach($cart as $a){
            $b = $a['price']*$a['SL'];
            $priceCart = $priceCart + $b;
        }
        if($request->id ){
            $cart = session()->get('cart');
            unset($cart[$request->id]);
            session()->put('cart',$cart);
            $cart = session()->get('cart');
            $listcart = view('pages.GioHang.show_cart',compact('cart','priceCart'))->render();
            return response()->json(['show_cart'=>$listcart, 'id'=>$request->id, 'code'=>200],200);
        }
    }

    public function updateCart(Request $request)
    {
        if($request->id ){
            $priceCart =0;
            $cart = session()->get('cart');
            $cart[$request->id]['SL'] =  $request->quantity;
            session()->put('cart',$cart);
            $cart = session()->get('cart');
            $cart1 = session()->get('cart');
            foreach($cart1 as $a){
                 $b = $a['price']*$a['SL'];
                 $priceCart = $priceCart + $b;
            }
            $listcart = view('pages.GioHang.show_cart',compact('cart','priceCart'))->render();
            return response()->json(['show_cart'=>$listcart, 'id'=>$request->id, 'code'=>200],200);
        }
    }

}
