<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Bill;
use App\Cart;
use App\Products;
use Carbon\Carbon;

class PaymentController extends Controller
{
    private $bill;

    public function __construct( Bill $bill)
    {
        $this->bill = $bill;
    }
    
    public function index(Request $request){
        $priceCart =0;
        $user = Auth::user();
        $sl = 0;
        $carts = session()->get('cart');
        $key_id = Hash::make(Carbon::now());
        foreach($carts as $a){
            //cap nhat cart
             $b = $a['price'] * $a['SL'];
            $priceCart= $priceCart+ $b;
            //cap nhat SL san pham
            $product = Products::where('id',$a['id'])->get();
            foreach ($product as $value) {
                $sl = $value->SL - $a['SL'];
                $update = Products::find($a['id']);
                $update->Sl = $sl;
                $update->save();
            }
        }
        //cap nhat bill
        $bill = new Bill();
        $bill->user_id = $user->id;
        $bill->status = 1;
        $bill->address = $request->xa. '-' .$request->huyen .'-' .$request->tinh;
        $bill->phone = $request->phone;
        $bill->money = $priceCart;
        $bill->time =Carbon::now();
        $bill->save();
        //cap nhan biil-product
        foreach($carts as $item){
            $bill->bill_products()->create([
                'product_id'=>$item['id'],
                'SL'=>$item['SL']
            ]);
        }

        session()->forget('cart');
        
        return view('pages.ThanhToan.hancash_checkout');
    }
}
