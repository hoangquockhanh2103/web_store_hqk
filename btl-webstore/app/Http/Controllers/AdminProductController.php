0<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Products;
use App\Recusive;
use App\Category;
use App\Products_image;
//use App\Http\Requests\ProductAddRequest;
use Storage;
use App\Traits\StorageImageTrait;

class AdminProductController extends Controller
{
    private $category;
    private $product;
    use StorageImageTrait;

    public function __construct( Category $category, Products $product, Products_image $products_image)
    {
        $this->product = $product;
        $this->products_image = $products_image;
        $this->category = $category;
    }

    public function index()
    {
        $products = $this->product->latest()->paginate(5);
        //dd($products);
        return view('admin.product.index',compact('products'));
    }

    public function create()
    {
        $htmlOption = $this->getCategory($parentId='');
        return view('admin.product.add',compact('htmlOption'));
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if($request->category_id == 0){
                $category_id = 1;
            }
            else $category_id = $request->category_id;
            $dataProductCreate = [
                'name' =>$request->name,
                'SL'=> $request->SL,
                'price' =>$request->price,
                'company_id'=>1,
                'size' =>$request->size,
                'color'=>$request->color,
                'chatLieu'=>$request->chatLieu,
                'description'=>$request->description,
                'category_id'=>$category_id
            ];
            $dataUploadFile = $this->storageTraitUpload($request, 'image','product');
            if(!empty($dataUploadFile)){
                $dataProductCreate['image'] = $dataUploadFile['file_name'];
                $dataProductCreate['image_path'] = $dataUploadFile['file_path'];
            }
            $product = $this->product->create($dataProductCreate);

            //inser image to table product_image
            if( $request->hasFile('image_path') ){
                foreach( $request->image_path as $fileItem ){
                    $dataProductImageDetail = $this->storageTraitUploadMultiple($fileItem,'product');

                    $product->images()->create([
                        'image_path'=>$dataProductImageDetail['file_path'],
                        'image_name'=>$dataProductImageDetail['file_name']
                    ]);
                }
            }
            DB::commit();
            return redirect()->route('product.index');
            
        } catch (Exception $e) {
            DB::rollBack();
            log::error('message: '.$e->getMessage().'line : '.$e->getLine());
        }
    }

    public function edit($id)
    {
        $products = $this->product->find($id);
        $htmlOption = $this->getCategory($products->category_id);
        return view('admin.product.edit',compact('products','htmlOption'));
    }
    public function delete($id)
    {
        try {
            $products = $this->product->find($id);
            $this->product->find($id)->delete();
            $this->products_image->where('product_id',$id)->delete();
            return response()->json([
                'code'=>200,
                'message'=>'success',
                'name'=>$products->name
            ],200);
            
        } catch (Exception $e) {
            log::error('message: '.$e->getMessage().'line : '.$e->getLine());
            return response()->json([
                'code'=>500,
                'message'=>'fail'
            ],500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $dataProductUpdate = [
                'name' =>$request->name,
                'SL'=> $request->SL,
                'price' =>$request->price,
                'size' =>$request->size,
                'color'=>$request->color,
                'chatLieu'=>$request->chatLieu,
                'description'=>$request->description,
                'category_id'=>$request->category_id
            ];
            $dataUploadFile = $this->storageTraitUpload($request, 'image','product');
            //dd($dataUploadFile);
            if(!empty($dataUploadFile)){
                $dataProductUpdate['image'] = $dataUploadFile['file_name'];
                $dataProductUpdate['image_path'] = $dataUploadFile['file_path'];
            }
            $this->product->find($id)->update($dataProductUpdate);
            $product = $this->product->find($id);

            //chèn hình ảnh vào bảng product_images
            if( $request->hasFile('image_path') ){
                
                $this->products_image->where('product_id',$id)->delete();

                foreach( $request->image_path as $fileItem ){
                    $dataProductImageDetail = $this->storageTraitUploadMultiple($fileItem,'product');

                    $product->images()->create([
                        'image_path'=>$dataProductImageDetail['file_path'],
                        'image_name'=>$dataProductImageDetail['file_name']
                    ]);
                }
            }
            DB::commit();
            return redirect()->route('product.index');
            
        } catch (Exception $e) {
            DB::rollBack();
            log::error('message: '.$e->getMessage().'line : '.$e->getLine());
        }
    }

    public function getCategory($parentId){
        $data = $this->category->all();
        $recusive = new Recusive($data);
        $htmlOption = $recusive->categoryRecusive($parentId);

        return $htmlOption;
    }

    function categoryRecusive($id,$text = ''){
        $data =  Category::all();

        foreach($data as $value){
            if ($value['parent_id'] == $id) {
                $this->htmlSelect .= "<option value=".$value['id'].">".$text .$value["name"]. "</option>";
                $this->categoryRecusive($value['id'], $text.'--');
            }
        }

        return $this->htmlSelect;
    }

}
