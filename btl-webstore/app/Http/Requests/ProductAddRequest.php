<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|unique:products|max:255|min:10',
            'price'=>'required',
            'category_id'=>'required',
            'description'=>'required',
            'image'=>'required',
            'SL'=>'required',
            'size'=>'required',
            'color'=>'required',
            'chatLieu'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống!',
            'name.unique'=>'Tên sản phẩm đã tồn tại',
            'name.max'=>'Tên không được dài quá 255 ký tự!',
            'name.min'=>'Tên không được ngắn hơn 10 ký tự!',
            'price.required'  => 'Giá không được để trống!',
            'category_id.required'=>'Danh mục không được để trống!',
            'description.required'=>'Mô tả không được để trống!',
            'image.required'=>'Ảnh đại diện không được để trống!',
            'SL.required'=>'Số lượng không được để trống!',
            'size.required'=>'Kích thước không được để trống!',
            'color.required'=>'Màu sắc không được để trống!',
            'chatLieu.required'=>'Chất liệu không được để trống!',
        ];
    }
}
