<?php 
namespace App\Traits;
use Storage;

trait StorageImageTrait{
	public function storageTraitUpload($request,$fileName,$folderName){
		if($request->hasFile($fileName)){
			$file = $request->$fileName;
	        $fileNameOriginal = $file->getClientOriginalName();
	        $fileNameHash = str_random(20) . '.' . $file->getClientOriginalExtension();
	        $filePath = $request->file($fileName)->storeAs('public/'.$folderName.'/'. auth()->id(), $fileNameHash);
	        $dataUpload =[
	            'file_name'=>$fileNameOriginal,
	            'file_path'=>Storage::url($filePath)
	        ];

        	return $dataUpload;
		}
		return null;

	}
	public function storageTraitUploadMultiple($file,$folderName){
				
	        $fileNameOriginal = $file->getClientOriginalName();
	        $fileNameHash = str_random(20) . '.' . $file->getClientOriginalExtension();
	        $filePath = $file->storeAs('public/'.$folderName.'/'. auth()->id(), $fileNameHash);
	        $dataUpload =[
	            'file_name'=>$fileNameOriginal,
	            'file_path'=>Storage::url($filePath)
	        ];

        	return $dataUpload;

	}
}