<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];
    public function productCart(){
        return $this->belongsTo(Products::class, 'product_id');
    }
    public function userCart(){
        return $this->belongsTo(User::class,'user_id');
    }
    
}
