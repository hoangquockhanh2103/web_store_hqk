<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Laravel\Scout\Searchable;
class Products extends Model
{
  use SoftDeletes;
  //use Searchable;
  protected $guarded = [];
  protected $table = 'products';

  public function images(){
    return $this->hasMany(Products_image::class,'product_id');
  }

  public function category(){
    return $this->belongsTo(Category::class, 'category_id');
  }

  public function productsImages()
  {
    return $this->hasMany(Products_image::class,'product_id');
  }
}
