<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $fillable = ['name','parent_id'];
    protected $table = 'categories';
    public $timestamps = false;
    use SoftDeletes;

    public function categoryChil()
    {
        return $this->hasMany(Category::class,'parent_id');
    }
    public function productChil()
    {
        return $this->hasMany(Products::class,'category_id');
    }
}
