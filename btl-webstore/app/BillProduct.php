<?php

namespace App;

use App\Products;
use Illuminate\Database\Eloquent\Model;

class BillProduct extends Model
{
    protected $guarded = [];
    public function products(){
        return $this->belongsTo(Products::class,'product_id');
    }
    public function productbill(){
        return $this->belongsTo(Products::class,'product_id');
    }
}
