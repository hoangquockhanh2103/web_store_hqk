<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Middleware\Checklogin;
// Route::get('/admin', ['as'=>'login.login','uses'=> 'HomeController@login']);
// Route::get('/admin-login', 'AdminController@loginAdmin');
// Route::post('/admin-login',[
//     'as'=>'login.login',
//     'uses'=> 'AdminController@postLoginAdmin'
// ]);
Route::get('/admin-login', function () {
        return view('login');
    })->name('login');;
Route::post("/admin-login",function(){
    $user = Request::get("user");
    $password = Request::get("password");
    if(Auth::attempt(array("user"=>$user,"password"=>$password))){
        return redirect("/admin/categories");
    }

    else{
        return redirect("/admin-login");
    }
});
Route::get('/login', function () {
        return view('login');
    })->name('login');;
Route::post("/login",function(){
    $user = Request::get("user");
    $password = Request::get("password");
    if(Auth::attempt(array("user"=>$user,"password"=>$password))){
        return redirect("/");
    }

    else{
        return redirect("/login");
    }
});
Route::get('logout', 'AdminController@logout');
Route::get('/', ['as'=>'home.index','uses'=>'HomeController@index']);
Route::prefix('/')->middleware(Checklogin::class)->group(function(){
    Route::get('/gio-hang', ['as'=>'carts.index','uses'=>'CartController@index']);
    Route::get('/chi-tiet-san-pham', [
        'as' => 'sanpham.details',
        'uses'=>'ProductController@details']);

    Route::post('/thanh-toan', [
        'as'=>'thanhtoan.index',
        'uses'=>'PaymentController@index'
    ]);
    Route::get('/deleteCart', [
        'as' => 'cart.deleteCart',
        'uses'=>'CartController@deleteCart']);

    Route::get('/updateCart', [
        'as' => 'cart.updateCart',
        'uses'=>'CartController@updateCart']);


    Route::get('/thong-tin-ca-nhan', [
        'as' => 'user.detailUser',
        'uses'=>'UserController@detailUser']);

    Route::get('/thong-tin-don_hang/{id}', [
        'as' => 'user.detailBill',
        'uses'=>'UserController@detailBill']);

    Route::post('/cap-nhat-tai-khoan/{id}', [
        'as' => 'user.updateuser',
        'uses'=>'UserController@updateuser']);

    Route::get('/cap-nhat-mat-khau/{id}', [
        'as' => 'user.changePass',
        'uses'=>'UserController@changePass']);

    Route::POST('/updateUserPass/{id}', [
        'as' => 'user.updateUserPass',
        'uses'=>'UserController@updateUserPass']);
});
Route::get('/Tim-kiem', [
        'as' => 'product.search',
        'uses'=>'HomeController@search']);

Route::get('/add-To-Cart/{id}', [
        'as' => 'cart.addToCart',
        'uses'=>'ProductController@addToCart']);



Route::get('/san-pham', [
        'as' => 'sanpham.index',
        'uses'=>'ProductController@index']);

Route::get('/san-pham/{id}', [
        'as' => 'sanpham.detail',
        'uses'=>'ProductController@detail']);

Route::get('/danhmuc/{id}', [
        'as' => 'danhmuc.show',
        'uses'=>'CategoryController@show']);

Route::prefix('admin')->group(function () {
    Route::prefix('categories')->group(function () {
        Route::get('/',[
            'as' => 'categories.index',
            'uses' => 'CategoryController@index',
            'middleware'=> 'can:category_list'
        ] );
        Route::get('/create',[
            'as' => 'categories.create',
            'uses' => 'CategoryController@create',
            'middleware'=> 'can:add_category'
        ] );
        Route::post('/store',[
            'as' => 'categories.store',
            'uses' => 'CategoryController@store',
            'middleware'=> 'can:add_category'
        ] );
        Route::get('/edit/{id}',[
            'as' => 'categories.edit',
            'uses' => 'CategoryController@edit',
            'middleware'=> 'can:edit_category'
        ] );
        Route::get('/delete/{id}',[
            'as' => 'categories.delete',
            'uses' => 'CategoryController@delete',
            'middleware'=> 'can:delete_category'
        ] );
        Route::post('/update/{id}',[
            'as' => 'categories.update',
            'uses' => 'CategoryController@update',
            'middleware'=> 'can:edit_category'
        ] );
    });
    Route::prefix('users')->group(function () {
         Route::get('/',[
            'as' => 'users.index',
            'uses' => 'UserController@index',
            'middleware'=> 'can:list_user'
        ] );
         Route::get('/create',[
            'as' => 'users.create',
            'uses' => 'UserController@create',
            'middleware'=> 'can:add_user'
        ] );
        Route::post('/store',[
            'as' => 'users.store',
            'uses' => 'UserController@store',
            'middleware'=> 'can:add_user'
        ] );
        Route::get('/edit/{id}',[
            'as' => 'users.edit',
            'uses' => 'UserController@edit',
            'middleware'=> 'can:edit_user'
        ] );
        Route::post('/update/{id}',[
            'as' => 'users.update',
            'uses' => 'UserController@update',
            'middleware'=> 'can:edit_user'
        ] );
        Route::get('/delete/{id}',[
            'as' => 'users.delete',
            'uses' => 'UserController@delete',
            'middleware'=> 'can:delete_user'
        ] );
    });
    Route::prefix('product')->group(function () {
        Route::get('/',[
            'as' => 'product.index',
            'uses' => 'AdminProductController@index',
            'middleware'=> 'can:list_product'
        ] );
        Route::get('/create',[
            'as' => 'product.create',
            'uses' => 'AdminProductController@create',
            'middleware'=> 'can:add_product'
        ] );
        Route::post('/store',[
            'as' => 'product.store',
            'uses' => 'AdminProductController@store',
            'middleware'=> 'can:add_product'
        ] );
        Route::get('/edit/{id}',[
            'as' => 'product.edit',
            'uses' => 'AdminProductController@edit',
            'middleware'=> 'can:edit_product'
        ] );
        Route::post('/update/{id}',[
            'as' => 'product.update',
            'uses' => 'AdminProductController@update',
            'middleware'=> 'can:edit_product'
        ] );
        Route::get('/delete/{id}',[
            'as' => 'product.delete',
            'uses' => 'AdminProductController@delete',
            'middleware'=> 'can:delete_product'
        ] );
    });
    Route::prefix('bill')->group(function () {
        Route::get('/',[
            'as' => 'bill.index',
            'uses' => 'AdminBillController@index'
        ] );
        Route::get('/detail/{id}',[
            'as' => 'bill.detail',
            'uses' => 'AdminBillController@detail'
        ] );
        Route::get('/update/{id}',[
            'as' => 'bill.update',
            'uses' => 'AdminBillController@update'
        ] );
    });
    Route::prefix('cart')->group(function () {
        Route::get('/',[
            'as' => 'cart.index',
            'uses' => 'AdminCartController@index'
        ] );
    });
    Route::prefix('roles')->group(function () {
        Route::get('/',[
            'as' => 'roles.index',
            'uses' => 'AdminRoleController@index',
            'middleware'=> 'can:list_role'
        ] );
        Route::get('/delete/{id}',[
            'as' => 'roles.delete',
            'uses' => 'AdminRoleController@delete',
            'middleware'=> 'can:delete_role'
        ] );
        Route::get('/create',[
            'as' => 'roles.create',
            'uses' => 'AdminRoleController@create',
            'middleware'=> 'can:add_role'
        ] );
        Route::post('/store',[
            'as' => 'roles.store',
            'uses' => 'AdminRoleController@store',
            'middleware'=> 'can:add_role'
        ] );
        Route::get('/edit/{id}',[
            'as' => 'roles.edit',
            'uses' => 'AdminRoleController@edit',
            'middleware'=> 'can:edit_role'
        ] );
        Route::post('/update/{id}',[
            'as' => 'roles.update',
            'uses' => 'AdminRoleController@update',
            'middleware'=> 'can:edit_role'
        ] );
    });
});

Auth::routes();
