@extends('layouts.home')
@section('content')
<div class="features_items"><!--features_items-->

        <div class="brands_products"><!--brands_products-->
            <h2>Nhãn Hiệu</h2>
            <div class="brands-name">
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="#"> <span class="pull-right">(50)</span>Nike</a></li>
                    <li><a href="#"> <span class="pull-right">(56)</span>Adidas</a></li>
                    <li><a href="#"> <span class="pull-right">(27)</span>Li-ning</a></li>
                    <li><a href="#"> <span class="pull-right">(32)</span>Underfeated</a></li>
                    <li><a href="#"> <span class="pull-right">(5)</span>Cactus Jack</a></li>
                    <li><a href="#"> <span class="pull-right">(9)</span>NOCTA</a></li>
                </ul>
            </div>
        </div><!--/Danh mục SP-->
                        
                        <div class="price-range"><!--Lọc theo giá-->
                            <h2>Lọc theo giá</h2>
                            <div class="well text-center">
                                 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="350000000" data-slider-step="100000" data-slider-value="[250,450]" id="sl2" ><br />
                                 <b class="pull-left">VND 0</b> <b class="pull-right">350 000 000</b>
                            </div>
                        </div><!--/Lọc theo giá-->

                        <div class="choose">
                            <ul class="nav nav-pills nav-justified">
                                <li><a href="#"><i class="fa fa-plus-square"></i>Yêu thích</a></li>
                                <li><a href="#"><i class="fa fa-plus-square"></i>So sánh</a></li>
                            </ul>
                        </div>
@endsection