@extends('home')
<style type="text/css">
	.user_info .single_field {
    width: 46% !important;
}
.cart_delete a{
	color: red !important;
}
</style>
@section('js')
	<script src="{{ asset('styleAdmin/index/addToCart.js') }}"></script>
@endsection
@section('content')

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="">Home</a></li>
				  <li class="active">Giỏ Hàng</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table data-url="{{ route('cart.deleteCart') }}" class="delete-cart table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Sản phẩm</td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng tiền</td>
							<td></td>
						</tr>
					</thead>
					<tbody data-url="{{ route('cart.updateCart') }}" class="update-cart">
						@if($cart != null)
							@foreach($cart as $id=> $item)
							<tr class="{{ $id }}">
								<td class="cart_description">
									<h4>{{ $item['name'] }}</h4>
									<p></p>
								</td>
								<td class="cart_price">
									<p>VND {{ number_format($item['price']) }}</p>
								</td>
								<td data-th="quantity" class="cart_quantity">
									<input type="number" class="quantity" id="quantity" value="{{ $item['SL'] }}"name="quantity">
								</td>
								<td class="cart_total">
									<p class="cart_total_price">VND {{ number_format($item['price']*$item['SL']) }}</p>
								</td>
								<td class="cart_delete">
									<a class="cart_quantity_delete cart-delete" data-id={{$id}} href=""><i class="fa fa-times"> Xóa</i></a>
									<a class="cart_quantity_delete cart-update" data-id={{$id}} href=""><i class="fa fa-times"> Cập nhật</i></a>
								</td>
							</tr>
							@endforeach
						@else
							<td>Không có sản phẩm nào trong giỏ hàng</td>
						@endif
						
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
	@if($cart != null)
	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>Thông tin đơn hàng</h3>
			</div>
			<form action="{{ route('thanhtoan.index') }}" method="POST" accept-charset="utf-8">
				@csrf
				<div class="row">
				<div class="col-sm-6">
					<div class="chose_area row">
						<div class="col-md-12">
							<ul class="user_info">
								<li class="single_field">
									<label>Tỉnh/Thành phố</label>
									<input type="text" name="tinh" required>
								</li>
								<li class="single_field">
									<label>Huyện/Quận</label>
									<input type="text" name="huyen" required>
								</li>
							</ul>
						</div>
						<div class="col-md-12">
							<ul class="user_info">
								<li class="single_field">
									<label>Xã</label>
									<input type="text" name="xa" required>
								</li>
								<li class="single_field">
									<label>SĐT liên lạc</label>
									<input type="number" maxlength="10" name="phone" required>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Tổng <span >VND {{ number_format($priceCart) }}</span></li>
							<li>Phí vận chuyển <span>Miễn phí</span></li>
							<li>Thanh toán <span name="price">VND {{ number_format($priceCart) }}</span></li>
						</ul>
							<button type="submit" class="btn btn-default check_out" href="">Thanh toán </button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</section>
	@endif
	<script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
	<script src="{{ asset('js/price-range.js') }}"></script>
    <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
@endsection