@extends('home')
@section('content')

<div class="product-details"><!--product-details-->
	<div class="col-sm-5">
		<div class="view-product">
			<img src="{{ $products->image_path }}" alt="" />
		</div>
		<div id="similar-product" class="carousel slide" data-ride="carousel">
			
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<!-- @foreach($products->productsImages as $item)
					<a href=""><img src="{{ $item->image_path }}" alt="{{ $item->image_name }}"></a>
					@endforeach -->
				</div>			
			</div>
			<!-- Controls -->
			<a class="left item-control" href="#similar-product" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="right item-control" href="#similar-product" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
	</div>
	<div class="col-sm-7">
		<div class="product-information"><!--/product-information-->
		<h2></h2>
		<h1>{{$products->name}}</h1>
		<span>
			<span value="">VND {{ number_format($products->price) }}</span>
			
			<button type="button" class="btn btn-fefault cart">
			<i class="fa fa-shopping-cart"></i>
			Thêm vào giỏ hàng
			</button>
		</span>
		<p><b>Số lượng: </b>{{ $products->SL }}</p>
		<p><b>Tình trạng:</b>
			@if($products->SL > 5)
				Còn hàng
			@elseif($products->SL = 0)
				Hết hàng
			@else
				Sắp hết
			@endif
		</p>
		<p><b>Màu sắc: </b>{{ $products->color }}</p>
		<p><b>Chất liệu: </b>{{ $products->chatLieu}}</p>
		<p><b>Kích thước: </b>{{ $products->size }}</p>
		<a href=""><img src="{{ asset('images/product-details/share.png') }}" class="share img-responsive"  alt="" /></a>
		</div><!--/product-information-->
	</div>
	
</div><!--/product-details-->

<div class="category-tab shop-details-tab"><!--category-tab-->
	<div class="col-sm-12">
		<ul  class="nav nav-tabs">
			<li class="active"><a href="#details" data-toggle="tab">Chi tiết</a></li>
		</ul>
	</div>
<div  class="tab-content">
	<div class="tab-pane fade active in" id="details" >
		<span>

		{!!html_entity_decode($products->description)!!}
			
		</span>
	</div>
</div><!--/category-tab-->
	
	<div class="recommended_items"><!--recommended_items-->
	<h2 class="title text-center">Sản phẩm khác</h2>
	
	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
				@foreach($productall as $item)
				<div class="col-sm-3">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<a href="{{ route('sanpham.detail',['id'=>$item->id]) }}">
									<img  src="{{ $item->image_path }}" alt="" />
								</a>
								<h2>VND {{ number_format($item->price) }}</h2>
								<a href="{{ route('sanpham.detail',['id'=>$item->id]) }}">
									<h4>{{$item->name}}</h4>
								</a>
								<!-- <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</button> --> 

								<!-- <a href="{{ route('cart.addToCart',['id'=>$item->id]) }}" class="btn btn-default add-to-cart addToCart"><i class="fa  fa-shopping-cart"></i>Thêm vào giỏ hàng</a> -->
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
</div>
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
<script src="{{ asset('js/price-range.js') }}"></script>
<script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
@endsection