@extends('home')
@section('js')
    <script src="{{ asset('styleAdmin/index/addToCart.js') }}"></script>

@endsection
@section('content')
<div class="features_items"><!--Sản phẩm-->
	<h2 class="title text-center">Sản phẩm</h2>
	@foreach($ds as $item)
	<div class="col-sm-3">
		<div class="product-image-wrapper">
			<div class="single-products">
				<div class="productinfo text-center">
					<a href="{{ route('sanpham.detail',['id'=>$item->id]) }}">
						<img  src="{{ $item->image_path }}" alt="" />
					</a>
					<h2>VND {{ number_format($item->price) }}</h2>
					<a href="{{ route('sanpham.detail',['id'=>$item->id]) }}">
						<h4>{{$item->name}}</h4>
					</a>
					<a data-url="{{ route('cart.addToCart',['id'=>$item->id]) }}" class="btn btn-default add-to-cart addToCart"><i class="fa  fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
				</div>
			</div>
		</div>
	</div>
	@endforeach
	<div style="text-align: center;" class="col-md-12">
		
	</div>
</div><!--Sản phẩm-->
<script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('js/price-range.js') }}"></script>
    <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
@endsection