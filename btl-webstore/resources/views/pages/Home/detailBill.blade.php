@extends('home')
<style type="text/css">
  .infor_name label{
    text-align: right;
  }
  .infor_name{
    height: 50px;
  }
  .infor_name input{
    height: 35px;
    width: 200px;
  }
  .form_update button {
    width: 70px;
    margin-bottom: 3.75rem;
    margin-left: calc(20% + 1.25rem)
  }
</style>
@section('content')
<div class="container">
    <div class="row">
      @foreach($user as $a)
        <form  method="get" accept-charset="utf-8">
          @csrf
          <div class="col-md-4 form_update ">
            <h2>Thông tin khách hàng</h2>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Họ và tên:</label>                
              <input type="text" value="{{ $a->full_name }}" disabled required/>
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Email: </label>                
              <input type="text" value="{{ $a->email }}"disabled required/>
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Số điện thoại: </label>                
              <input type="number" value="0{{ $a->phone_number }}"disabled required/>
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Địa chỉ :</label>                
              <input type="text" value="{{ $a->address }}"disabled required />
            </div>
            <button type="submit" class="btn btn-danger">Lưu</button>
          </div>
        </form>
      @endforeach
      <div class="col-md-8">
        <h2>Thông tin về đơn hàng</h2>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Ngày mua </th>
                <th scope="col">Tên sản phẩm</th>
                <th scope="col">Giá sản phẩm</th>
                <th scope="col">Số lượng</th>
              </tr>
            </thead>
            <tbody>
              @foreach($carts as $item)
              <tr>
                <td >{{ date_format($item->created_at,"d/m/Y") }}</td>
                <td >{{ $item->productCart->name }}</td>
                <td >{{ number_format($item->productCart->price) }}</td>
                <td >{{ number_format($item->SL) }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
      </div>
    </div>
</div>
@endsection