@extends('home')
<style type="text/css">
  .infor_name label{
    text-align: right;
  }
  .infor_name{
    height: 50px;
  }
  .infor_name input{
    height: 35px;
    width: 200px;
  }
  .save {
    width: 70px;
    margin-bottom: 3.75rem;
    margin-left: calc(20% + 1.25rem)
  }
</style>
@section('content')
<div class="container">
    <div class="row">
      @foreach($user as $a)
        <form action="{{ route('user.updateuser',['id'=>$a->id]) }}" method="POST" accept-charset="utf-8">
           @csrf
          <div class="col-md-4 form_update ">
            <h2>Thông tin khách hàng</h2>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Họ và tên:</label>                
              <input type="text" name="full_name" value="{{ $a->full_name }}" required/>
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Email: </label>                
              <input type="text" name="email" value="{{ $a->email }}" required/>
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Số điện thoại: </label>                
              <input type="number" name="phone_number" value="0{{ $a->phone_number }}" required/>
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Địa chỉ :</label>                
              <input type="text" name="address" value="{{ $a->address }}" required />
            </div>
            <button type="submit" class="btn btn-danger save">Lưu</button>
            <button class="btn btn-danger "><a href="{{ route('user.changePass',['id'=>$a->id]) }}">
              Thay đổi mật khẩu
            </a></button>
          </div>
        </form>
      @endforeach
      <div class="col-md-8">
        <h2>Thông tin về đơn hàng</h2>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Ngày mua </th>
                <th scope="col">Tổng tiền</th>
                <th scope="col">Trạng thái đơn hàng</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($bills as $item)
              <tr>
                <td >{{ date_format($item->created_at,"d/m/Y") }}</td>
                <td >{{ number_format($item->money) }}</td>
                @if($item->status === 1)
                  <td >Đang đợi xét duyệt</td>
                @elseif($item->status === 2)
                  <td >Đã duyệt đơn hàng</td>
                @elseif($item->status === 3)
                  <td>Đang giao hàng</td>
                @elseif($item->status === 4)
                  <td >Giao hàng thành công</td>
                @endif  
                <td>
                  <a href="{{ route('user.detailBill',['id'=> $item->id]) }}" class="btn btn-default">Chi tiết</a>
                </td>     
              </tr>
              @endforeach
            </tbody>
          </table>
      </div>
    </div>
</div>
@endsection