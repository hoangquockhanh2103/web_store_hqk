@extends('home')
@section('js')
	<script src="{{ asset('styleAdmin/index/addToCart.js') }}"></script>
@endsection
<style type="text/css"> 
	
	.item{
		padding: 0 !important;
	}
	.item img{
		height: 400px !important;
	}
</style>
@section('content')
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Danh Mục</h2>
						@foreach($categories as $category)
						<div class="panel-group category-products" id="danhmuc"><!--DanhMuc SP-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#danhmuc" href="#{{ $category->id }}">
											@if($category->categoryChil->count())
												<span class="badge pull-right">
													<i class="fa fa-plus"></i>
												</span>
											@endif
											<a href="{{ route('danhmuc.show',['id'=>$category->id]) }}">{{ $category->name }}</a>
										</a>
									</h4>
								</div>
								<div id="{{ $category->id }}" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											@foreach($category->categoryChil as $item)
											<li><a href="{{ route('danhmuc.show',['id'=>$item->id]) }}">{{ $item->name }} </a></li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
						</div><!--/category-products-->
						@endforeach
					</div>
				</div>
				<div class="col-sm-9">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-12">
									<img src="images/home/nike1.jpg" class="noithat img-responsive" alt="" />
								</div>
							</div>
							<div class="item">
								<img src="images/home/nike2.png" class="noithat img-responsive" alt="" />
							</div>
							<div class="item col-sm-12">
								<img src="images/home/nike3.jpg" class="noithat img-responsive" alt="" />
							</div>
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
	
	<section>
		<div class="container">
			<div class="row">				
				<div class="col-sm-12 padding-right">
					<div class="features_items"><!--Sản phẩm-->
						<h2 class="title text-center">Sản phẩm</h2>
						@foreach($product as $item)
						<div class="col-sm-3">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<a href="{{ route('sanpham.detail',['id'=>$item->id]) }}"><img  src="{{ $item->image_path }}" alt="" /></a>
											<h2>VND {{ number_format($item->price) }}</h2>
											<a href="{{ route('sanpham.detail',['id'=>$item->id]) }}"><h4>{{$item->name}}</h4></a>
											<a data-url="{{ route('cart.addToCart',['id'=>$item->id]) }}" class="btn btn-default add-to-cart addToCart"><i class="fa  fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
										</div>
								</div>
							</div>
						</div>
						@endforeach
					</div><!--Sản phẩm-->
					
					<div class="category-tab"><!--tab sản phẩm-->
						@foreach($categories1 as $item)
						@if($item->productChil->count())
							<div class="col-sm-12">
								<ul class="nav nav-tabs">
										<li class="">
											<a href="{{ route('danhmuc.show',['id'=>$item->id]) }}">{{ $item->name }}</a>
										</li>
								</ul>
							</div>
						@foreach($item->productChil as $item2)
						<div class="tab-content">
							<div class="tab-pane fade active in"  >
								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<a href="{{ route('sanpham.detail',['id'=>$item2->id]) }}"><img  src="{{ $item2->image_path }}" alt="" /></a>
												<h2>VND {{ number_format($item2->price) }}</h2>
												<a href="{{ route('sanpham.detail',['id'=>$item2->id]) }}"><h4>{{$item2->name}}</h4></a>
												<a data-url="{{ route('cart.addToCart',['id'=>$item->id]) }}" class="btn btn-default add-to-cart addToCart"></i>Thêm vào giỏ hàng</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						@endforeach
						</div>
						@endif
					@endforeach
					</div><!--/Danh sách sản phẩm-->
				</div>
			</div>
		</div>
	</section>
  
    <script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
	<script src="{{ asset('js/price-range.js') }}"></script>
    <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
@endsection