@extends('home')
<style type="text/css">
  .infor_name label{
    text-align: right;
  }
  .infor_name{
    height: 50px;
  }
  .infor_name input{
    height: 35px;
    width: 200px;
  }
  .form_update button {
    width: 70px;
    margin-bottom: 3.75rem;
    margin-left: calc(20% + 1.25rem)
  }
</style>
@section('content')
<div class="container">
    <div class="row">
      @foreach($user as $a)
        <form action="{{ route('user.updateUserPass',['id'=>$a->id]) }}" method="POST" accept-charset="utf-8">  @csrf
          <div class="col-md-4 form_update ">
            <h2>Thông tin khách hàng</h2>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Mật khẩu cũ</label>                
              <input type="password" name="password" required/>
              @if(session('error'))<span style="color: red;"><br/>Mật khẩu không chính xác</span>
              @endif
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Mật khẩu mới: </label>                
              <input type="password" name="password_new"  required/> 
              @if(session('pass_confirm'))<span style="color: red;"><br/>Mật khẩu không chính xác</span>
              @endif
            </div>
            <div class="infor_name col-md-12">
              <label class="margin-left col-md-3">Nhập lại mật khẩu: </label>                
              <input type="password" name="password_confirm" required/>
            </div>
            <button type="submit" class="btn btn-danger">Lưu</button>
          </div>
        </form>
      @endforeach
      <div class="col-md-8">
        <h2>Thông tin về đơn hàng</h2>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Ngày mua </th>
                <th scope="col">Tổng tiền</th>
                <th scope="col">Trạng thái đơn hàng</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($bills as $item)
              <tr>
                <td >{{ date_format($item->created_at,"d/m/Y") }}</td>
                <td >{{ number_format($item->money) }}</td>
                @if($item->status === 1)
                  <td >Đang đợi xét duyệt</td>
                @elseif($item->status === 2)
                  <td >Đã duyệt đơn hàng</td>
                @elseif($item->status === 3)
                  <td>Đang giao hàng</td>
                @elseif($item->status === 4)
                  <td >Giao hàng thành công</td>
                @endif  
                <td>
                  <a href="{{ route('user.detailBill',['id'=> $item->id]) }}" class="btn btn-default">Chi tiết</a>
                </td>     
              </tr>
              @endforeach
            </tbody>
          </table>
      </div>
    </div>
</div>
@endsection