@extends('home')
@section('content')

<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Thanh Toán</li>
				</ol>
			</div>

			
			<div class="review-payment">
				<h2>Xem lại & Thanh toán</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Ảnh</td>
							<td class="description">Sản phẩm</td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@foreach($cart as $a)
						<tr>
							<td class="cart_product">
								<a  href=""><img style="height: 200px" src="{{ $a['image'] }}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{ $a['name'] }}</a></h4>
							</td>
							<td class="cart_price">
								<p>VND {{ number_format($a['price']) }}</p>
							</td>
							<td class="cart_quantity">
								<p>Số lượng: {{ $a['SL'] }}</p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">VND {{ number_format($a['price']*$a['SL']) }}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area row">
						<div class="col-md-12">
							<ul class="user_info">
								<li class="single_field">
									<label>Tỉnh/Thành phố</label>
									<input value="{{ $request->tinh }}" type="text" name="tinh" required>
								</li>
								<li class="single_field">
									<label>Huyện/Quận</label>
									<input value="{{ $request->huyen }}" type="text" name="huyen" required>
								</li>
							</ul>
						</div>
						<div class="col-md-12">
							<ul class="user_info">
								<li class="single_field">
									<label>Xã</label>
									<input value="{{ $request->xa }}" type="text" name="xa" required>
								</li>
								<li class="single_field">
									<label>SĐT liên lạc</label>
									<input value="{{ $request->sdt }}" type="number" name="sdt" required>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Tổng <span >VND {{ number_format($priceCart) }}</span></li>
							<li>Phí vận chuyển <span>Miễn phí</span></li>
							<li>Thanh toán <span name="price">VND {{ number_format($priceCart) }}</span></li>
						</ul>
							<button type="submit" class="btn btn-default check_out" href="">Đặt hàng </button>
					</div>
				</div>
			</div>
		</div>
	</section> <!--/#cart_items-->

@endsection