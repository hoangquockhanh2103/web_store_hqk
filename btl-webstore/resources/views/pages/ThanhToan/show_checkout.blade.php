@extends('layouts.home')
@section('content')

<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{URL::to('/')}}">Trang chủ</a></li>
				  <li class="active">Thanh toán</li>
				</ol>
			</div>

			<div class="register-req">
				<p>Hãy đăng ký hoặc đăng nhập để thanh toán giỏ hàng và xem lại lịch sử mua hàng</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						<div class="shopper-info">
							<p>Thông tin khách hàng</p>
							<form>
								<input type="text" placeholder="Tên hiển thị">
								<input type="text" placeholder="Tài khoản">
								<input type="password" placeholder="Mật khẩu">
								<input type="password" placeholder="Nhắc lại mật khẩu">
							</form>
							<a class="btn btn-primary" href="">Lấy trích dẫn</a>
							<a class="btn btn-primary" href="">Tiếp tục</a>
						</div>
					</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<p>Người thanh toán</p>
							<div class="form-one">
								<form>
									<input type="text" placeholder="Tên công ty">
									<input type="text" placeholder="Email*">
									<input type="text" placeholder="Tiêu đề">
									<input type="text" placeholder="Họ *">
									<input type="text" placeholder="Tên đệm">
									<input type="text" placeholder="Tên *">
									<input type="text" placeholder="Địa chỉ 1 *">
									<input type="text" placeholder="Địa chỉ 2">
								</form>
							</div>
							<div class="form-two">
								<form>
									<input type="text" placeholder="Vùng *">
									<select>
										<option>-- Quốc Gia --</option>
										<option>United States</option>
										<option>Thái Lan</option>
										<option>Anh</option>
										<option>Trung Quốc</option>
										<option>Hàn Quốc</option>
										<option>Việt Nam</option>
										<option>Campuchia</option>
										<option>Nơi khác</option>
									</select>
									<select>
										<option>-- Khu Vực --</option>
										<option>United States</option>
										<option>Thái Lan</option>
										<option>Anh</option>
										<option>Trung Quốc</option>
										<option>Hàn Quốc</option>
										<option>Việt Nam</option>
										<option>Campuchia</option>
										<option>Nơi khác</option>
									</select>
									<input type="password" placeholder="Nhắc lại mật khẩu">
									<input type="text" placeholder="SĐT *">
									<input type="text" placeholder="ĐTDD">
									<input type="text" placeholder="Fax">
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="order-message">
							<p>Đơn hàng</p>
							<textarea name="message"  placeholder="Ghi chú về đơn đặt hàng của bạn, Ghi chú đặc biệt khi giao hàng" rows="16"></textarea>
							<label><input type="checkbox"> Vận chuyển đến địa chỉ thanh toán</label>
						</div>	
					</div>					
				</div>
			</div>
			<div class="review-payment">
				<h2>Xem lại giỏ hàng</h2>
			</div>

			
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Thanh toán qua thẻ ngân hàng</label>
					</span>
					<span>
						<label><input type="checkbox"> Thanh toán khi nhận hàng</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->

@endsection