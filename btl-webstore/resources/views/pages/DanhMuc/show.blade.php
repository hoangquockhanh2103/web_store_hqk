@extends('home')
@section('js')
    <script src="{{ asset('styleAdmin/index/addToCart.js') }}"></script>
@endsection
@section('content')
<section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Danh Mục</h2>
                        @foreach($categories as $category)
                        <div class="panel-group category-products" id="danhmuc"><!--DanhMuc SP-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#danhmuc" href="#{{ $category->id }}">
                                            @if($category->categoryChil->count())
                                                <span class="badge pull-right">
                                                    <i class="fa fa-plus"></i>
                                                </span>
                                            @endif
                                            <a href="{{ route('danhmuc.show',['id'=>$category->id]) }}">{{ $category->name }}</a>
                                        </a>
                                    </h4>
                                </div>
                                <div id="{{ $category->id }}" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            @foreach($category->categoryChil as $item)
                                            <li><a href="{{ route('danhmuc.show',['id'=>$item->id]) }}">{{ $item->name }} </a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!--/category-products-->
                        @endforeach
                    </div>
                </div>
                
                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--Sản phẩm-->
                        <h2 class="title text-center">Sản phẩm</h2>
                        @foreach($showProducts as $item)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <a href="{{ route('sanpham.detail',['id'=>$item->id]) }}">
                                                <img  src="{{ $item['image_path'] }}" alt="" />
                                            </a>
                                            <h2>VND {{ number_format($item->price) }}</h2>
                                            <a href="{{ route('sanpham.detail',['id'=>$item->id]) }}">
                                                <h4>{{$item->name}}</h4>
                                            </a>
                                            <a data-url="{{ route('cart.addToCart',['id'=>$item->id]) }}" class="btn btn-default add-to-cart addToCart"><i class="fa  fa-shopping-cart"></i>Thêm vào giỏ hàng</a>
                                        </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div><!--Sản phẩm-->
                </div>
            </div>
        </div>
    </section>
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('js/price-range.js') }}"></script>
    <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
@endsection