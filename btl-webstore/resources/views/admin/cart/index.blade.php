@extends('layouts.admin')
@section('title')
<title>admin</title>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Giỏ Hàng','key' => 'Danh Sách'])
  
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-121">
          
        </div>
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Tên khách hàng</th>
                <th scope="col">Tên sản phẩm</th>
                <th scope="col">Số lượng</th>
              </tr>
            </thead>
            <tbody>
              @foreach($carts as $item)
              <tr>
                <th scope="row">{{ $item->order_id }}</th>
                <td >{{ $item->userCart->full_name }}</td>
                <td >{{ $item->productCart->name }}</td>
                <td >{{ number_format($item->SL) }}</td>
               <td>
                  <a href="" class="btn btn-default">Sửa</a>
                  <a data-url="" class="btn btn-danger action-delete">Xoá</a>
                </td>         
              </tr>
              @endforeach
            </tbody>
          </div>
        </table>
      </div>
      <div class="col-md-12">
        {{ $carts->links() }}
      </div>
    </div>
  </div>
</div>
</div>
@endsection