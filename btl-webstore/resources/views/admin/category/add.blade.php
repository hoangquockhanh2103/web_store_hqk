@extends('layouts.admin')
@section('title')
<title>admin</title>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Danh Mục','key' => 'Thêm'])

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <form action="{{ route('categories.store') }}" method="post">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Danh mục</label>
              <input type="text" 
                class="form-control" 
                placeholder="Nhập tên danh mục"
                name="name"
                >
            </div>
            <div class="form-group">
              <label>Danh mục cha</label>
              <select class="form-control" name="parent_id">
                <option value="0">Chọn danh mục cha</option>
                {!! $htmlOption !!}
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Thêm</button>
          </form>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection