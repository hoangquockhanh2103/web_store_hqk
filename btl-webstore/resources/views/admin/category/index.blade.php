@extends('layouts.admin')
@section('title')
<title>admin</title>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Danh Mục','key' => 'Danh Sách'])
  
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-121">
          <a href="{{ route('categories.create') }}" class="btn btn-success m-2">Thêm Mới</a>
        </div>
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Tên danh mục</th>
                <th scope="col">Ngày tạo</th>
                <th scope="col">Chức Năng</th>
              </tr>
            </thead>
            <tbody>
              @foreach($categories as $category)
              <tr>
                <th scope="row">{{ $category->id }}</th>
                <td>{{ $category->name }}</td>
                <td>{{ $category->created_at }}</td>
                <td>
                  <a href="{{ route('categories.edit',['id'=> $category->id]) }}" class="btn btn-default">Sửa</a>
                  <a href="{{ route('categories.delete',['id'=> $category->id]) }}" class="btn btn-danger">Xoá</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </div>
        </table>
      </div>
      <div class="col-md-12">
        {{ $categories->links() }}
      </div>
    </div>
  </div>
</div>
</div>
@endsection