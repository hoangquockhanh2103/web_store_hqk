@extends('layouts.admin')
@section('title')
<title>Sửa danh mục</title>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Danh Mục','key' => 'Sửa'])

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <form action="{{ route('categories.update',['id' => $category->id]) }}" method="post">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Danh mục</label>
              <input type="text" 
                class="form-control" 
                value="{{ $category->name }}" 
                placeholder="Nhập tên danh mục"
                name="name"
                >
            </div>
            <div class="form-group">
              <label>Danh mục cha</label>
              <select class="form-control" name="parent_id">
                <option value="0">Chọn danh mục cha</option>
                {!! $htmlOption !!}
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Lưu</button>
          </form>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection