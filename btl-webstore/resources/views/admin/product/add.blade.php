@extends('layouts.admin')
@section('title')
  <title>Thêm sản phẩm</title>
@endsection
@section('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Sản phẩm','key' => 'Thêm'])
  <div class="col-md-12">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  </div>
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label >Tên sản phẩm</label>
                  <input type="text" 
                    class="form-control" 
                    placeholder="Nhập tên sản phẩm"
                    name="name"
                    required 
                    >
                </div>
                <div class="form-group">
                  <label >Giá sản phẩm</label>
                  <input type="number" 
                    class="form-control" 
                    placeholder="Nhập giá sản phẩm"
                    name="price" required
                    >
                </div>
                <div class="form-group">
                  <label >Số lượng</label>
                  <input type="number" 
                    class="form-control" 
                    placeholder="Nhập giá sản phẩm"
                    name="SL" required
                    >
                </div>
                <div class="form-group">
                  <label >Ảnh đại diện sản phẩm</label>
                  <input type="file" 
                    class="form-control-file" 
                    name="image" required
                    >
                </div>
                <div class="form-group">
                  <label >Ảnh chi tiết sản phẩm</label>
                  <input type="file" 
                    class="form-control-file" 
                    name="image_path[]"
                    multiple 
                    >
                </div>
                <div class="form-group">
                  <label >Kích thước</label>
                  <input type="number" 
                    class="form-control" 
                    placeholder="Nhập kích thước"
                    name="size" required
                    >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label >Màu sắc</label>
                  <input type="text" 
                    class="form-control" 
                    placeholder="Nhập màu sắc"
                    name="color" required
                    >
                </div>
                <div class="form-group">
                  <label >Chất liệu</label>
                  <input type="text" 
                    class="form-control" 
                    placeholder="Nhập chất liệu"
                    name="chatLieu" required
                    >
                </div>
                <div class="form-group">
                  <label>Danh mục</label>
                  <select  class="form-control select2_init" name="category_id" required="required">
                    <option style="height: 37px;" value="0">Chọn danh mục</option>
                    {!! $htmlOption !!}
                  </select>
                </div>
                <div class="form-group">
                  <label >Mô tả</label>
                  <textarea type="text" 
                    class="form-control my-editor" 
                    name="description"
                    rows=4 required
                    >                    
                  </textarea> 
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Thêm</button>
          </form>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>
      $(function (){
        $(".select2_init").select2({
          
        });
      })
  </script>
  <script>
  let editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      let y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      let cmsURL = editor_config.path_absolute + 'filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection