@extends('layouts.admin')
@section('title')
  <title>Sản phẩm</title>
@endsection
@section('js')
  <script src="{{ asset('js-framework/sweetalert2@11.js') }}"></script>
  <script src="{{ asset('styleAdmin/index/list.js') }}"></script>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Sản phẩm','key' => 'Danh Sách'])
  
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-121">
          <a href="{{ route('product.create') }}" class="btn btn-success m-2">Thêm Mới</a>
        </div>
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tên sản phẩm</th>
                <th scope="col">Giá (vnđ)</th>
                <th scope="col">Số lượng</th>
                <!-- <th scope="col">Kích thước</th>
                <th scope="col">Màu sắc</th>
                <th scope="col">Chất liệu</th> -->
                <th scope="col">Danh mục</th>
                <!-- <th scope="col">Mô tả</th> -->
                <th scope="col">Hình ảnh</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
              @foreach($products as $productItem)
              <tr>
                <th scope="row">{{ $productItem->id }}</th>
                <td >{{ $productItem->name }}</td>
                <td >{{ number_format($productItem->price) }}</td>
                <td >{{ $productItem->SL }}</td>
                <!-- <td >{{ $productItem->size }}</td>
                <td >{{ $productItem->color }}</td>
                <td >{{ $productItem->chatLieu }}</td> -->
                <td >{{ optional($productItem->category)->name }}</td>
                <!-- <td >{{ strip_tags($productItem->description) }}</td> -->
                <td>
                  <img style="height: 100px; width: 100px " class="products-images" src="{{ $productItem->image_path }}" alt="">
                </td>
               <td>
                  <a href="{{ route('product.edit',['id'=>$productItem->id]) }}" class="btn btn-default">Sửa</a>
                  <a data-url="{{ route('product.delete',['id'=>$productItem->id]) }}" class="btn btn-danger action-delete">Xoá</a>
                </td>         
              </tr>
              @endforeach
            </tbody>
          </div>
        </table>
      </div>
      <div class="col-md-12">
        {{$products->links()}}
      </div>
    </div>
  </div>
</div>
</div>
@endsection