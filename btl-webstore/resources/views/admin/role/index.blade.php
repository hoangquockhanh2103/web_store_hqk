@extends('layouts.admin')
@section('title')
<title>Tài khoản</title>
@endsection
@section('js')
  <script src="{{ asset('js-framework/sweetalert2@11.js') }}"></script>
  <script src="{{ asset('styleAdmin/index/list.js') }}"></script>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Phân Quyền','key' => 'Danh Sách'])
  
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-121">
          <a href="{{ route('roles.create') }}" class="btn btn-success m-2">Thêm Mới</a>
        </div>
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Tên vai trò</th>
                <th scope="col">Mô tả vai trò</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
              @foreach($roles as $a)
              <tr>
                <th scope="row">{{ $a->id }}</th>
                <td>{{ $a->name }}</td>
                <td>{{ $a->display_name }}</td>
                <td>
                  <a href="{{ route('roles.edit',['id'=>$a->id]) }}" class="btn btn-default">Sửa</a>
                  <a data-url="{{ route('roles.delete',['id'=>$a->id]) }}" class="btn btn-danger action-delete">Xoá</a>
                </td>    
              </tr>
              @endforeach
            </tbody>
          </div>
        </table>
      </div>
      <div class="col-md-12">
         {{ $roles->links() }}
      </div>
    </div>
  </div>
</div>
</div>
@endsection