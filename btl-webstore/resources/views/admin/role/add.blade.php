@extends('layouts.admin')
@section('title')
<title>Thêm tài khoản</title>
@endsection

@section('content')
<div class="content-wrapper">
  @include('layouts.content-header',['name' => 'Phân Quyền','key' => 'Thêm'])
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <form action="{{ route('roles.store') }}" method="post" style="width: 100%;">
          <div class="col-md-8">
            @csrf
            <div class="form-group">
              <label >Tên vai trò</label>
              <input type="text"
              class="form-control"
              placeholder="Nhập tên vai trò "
              name="name"required
              >
            </div>
            <div class="form-group">
              <label >Mô tả vai trò</label>
              <textarea
              class="form-control"
              placeholder="Nhập mô tả"
              name="display_name"  rows=4 required></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              @foreach($permissionParent as $item)
              <div class="card border-primary mb-3 col-md-12 ">
                <div class="card-header">
                  <label><input type="checkbox" name="" class="checkbox_parent" value="{{ $item->id }}">
                    Module {{$item->name}}
                  </label>
                </div>
                <div class="row">
                  @foreach($item->permissionChil as $itemChil)
                  <div class="card-body text-primary col-md-3">
                    <h5 class="card-title">
                      <label><input type="checkbox" name="permission_id[]"class="checkbox_chil" value="{{ $itemChil->id }}">
                         {{$itemChil->name}}
                      </label>
                   </h5>
                  </div>
                  @endforeach
                </div>
              </div> 
              @endforeach           
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Thêm</button>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>
$(function (){
  $(".select2_init").select2({
    'placeholder':'Chọn vai trò'
  });
});
$('.checkbox_parent').on('click',function() {
    $(this).parents('.card').find('.checkbox_chil').prop('checked',$(this).prop('checked'));
  })
</script>
@endsection

@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
  .card-header{
    background-color: #33CC00;
    margin: 0;
  }
</style>
@endsection