@extends('layouts.admin')
@section('title')
<title>Thêm tài khoản</title>
@endsection
@section('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <style>
    .select2-selection__choice{
      background-color: #5897fb !important;
    }
  </style>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Tài khoản','key' => 'Thêm'])

  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <form action="{{ route('users.store') }}" method="post">
            @csrf
            <div class="form-group">
              <label >Tài khoản</label>
              <input type="text" 
                class="form-control" 
                placeholder="Nhập tên tài khoản "
                name="user"
                >
            </div>
            <div class="form-group">
              <label >Mật khẩu</label>
              <input type="password" 
                class="form-control" 
                placeholder="Nhập mật khẩu"
                name="password"
                >
            </div>
            <div class="form-group">
                  <label>Chọn vai trò</label>
                  <select  class="form-control select2_init" name="role_id[]" multiple>
                    <option style="height: 40px;" value="">Chọn quyền</option>
                    @foreach($roles as $item)
                      <option value="{{ $item->id }}">{{$item->name}}</option>
                    @endforeach
                  </select>
                </div>
            <div class="form-group">
              <label >Họ và tên</label>
              <input type="text" 
                class="form-control" 
                placeholder="Nhập họ và tên"
                name="full_name"
                >
            </div>
            <div class="form-group">
              <label >email</label>
              <input type="email" 
                class="form-control" 
                placeholder="Nhập email"
                name="email"
                >
            </div>
            <div class="form-group">
              <label>Số điện thoại</label>
              <input type="number" 
                class="form-control" 
                placeholder="Nhập số điện thoại"
                name="phone"
                >
            </div>
            <div class="form-group">
              <label>Địa chỉ</label>
              <input type="text" 
                class="form-control" 
                placeholder="Nhập địa chỉ"
                name="address"
                >
            </div>
            <button type="submit" class="btn btn-primary">Đăng ký</button>
          </form>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>
      $(function (){
        $(".select2_init").select2({
          'placeholder':'Chọn vai trò'
        });
      })
  </script>
  @endsection