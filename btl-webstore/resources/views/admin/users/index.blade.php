@extends('layouts.admin')
@section('title')
<title>Tài khoản</title>
@endsection
@section('js')
  <script src="{{ asset('js-framework/sweetalert2@11.js') }}"></script>
  <script src="{{ asset('styleAdmin/index/list.js') }}"></script>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Tài khoản','key' => 'Danh Sách'])
  
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-121">
          <a href="{{ route('users.create') }}" class="btn btn-success m-2">Thêm Mới</a>
        </div>
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Tên</th>
                <th scope="col">Họ và tên</th>
                <th scope="col">Địa chỉ</th>
                <th scope="col">Số điện thoại</th>
                <th scope="col">email</th>
                <th scope="col">Chức năng</th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
              <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->user }}</td>
                <td>{{ $user->full_name }}</td>
                <td>{{ $user->address }}</td>
                <td>{{ $user->phone_number }}</td>
                <td>{{ $user->email }}</td>
                <td>
                  <a href="{{ route('users.edit',['id'=>$user->id]) }}" class="btn btn-default">Sửa</a>
                  <a data-url="{{ route('users.delete',['id'=>$user->id]) }}" class="btn btn-danger action-delete">Xoá</a>
                </td>    
              </tr>
              @endforeach
            </tbody>
          </div>
        </table>
      </div>
      <div class="col-md-12">
         {{ $users->links() }}
      </div>
    </div>
  </div>
</div>
</div>
@endsection