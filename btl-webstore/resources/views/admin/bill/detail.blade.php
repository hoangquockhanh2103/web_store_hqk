@extends('layouts.admin')
@section('title')
<title>admin</title>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Hoá Đơn','key' => 'Danh Sách'])
  
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form action="{{ route('bill.update',[ 'id'=>$bills->id ]) }}">
            <div class="form-group">
              <label >Tên khách hàng: {{$user->full_name}}</label>
            </div>
            <div class="form-group">
              <label >Địa chỉ: {{ $bills->address }} </label>
            </div>
            <div class="form-group">
              <label >Số diện thoai: 0{{ $bills->phone }} </label>
            </div>
            <div class="form-group">
              <label>Trạng thái đơn hàng: </label>
              <select name="status">
              <option value={{ $bills->status }}>{{ $a }}</option>
              <option value="1">Đang chờ xét duyệt</option>
              <option value="2">Đã xét duyệt</option>
              <option value="3">Đang giao hàng</option>
              <option value="4">Giao hàng thành công</option>
            </select>
            </div>
            <div class="form-group">
              <label >Tổng tiền: {{number_format($bills->money) }} VNĐ </label>
            </div>
              <table class="table">
                <tbody>
                  <tr>
                    <th scope="col">Tên sản phẩm </th>
                    <th scope="col">Đơn giá</th>
                    <th scope="col">Số lượng</th>
                  </tr>
                </tbody>
                <thead>
                  @foreach($sp as $item)
                      <tr>
                        <td>{{$item->products->name}}</td>
                        <td>{{$item->products->price}}</td>
                        <td>{{$item->SL}}</td>
                      </tr>
                  @endforeach
                </thead>
              </table>
              <button type="submit" class="btn btn-primary">Cập nhật</button>
          </form>
      </div>
    </div>
  </div>
</div>
@endsection