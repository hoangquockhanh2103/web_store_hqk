@extends('layouts.admin')
@section('title')
<title>admin</title>
@endsection
@section('content')
<div class="content-wrapper">

  @include('layouts.content-header',['name' => 'Hoá Đơn','key' => 'Danh Sách'])
  
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-121">
        </div>
        <div class="col-md-12">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Tên </th>
                <th scope="col">Số Tiền</th>
                <th scope="col">Trạng thái</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($bills as $item)
              <tr>
                <th scope="row">{{ $item->id }}</th>
                <td >{{ $item->userBill->full_name }}</td>
                <td >{{ number_format($item->money) }}</td>
                @if($item->status === 1)
                  <td >Đang đợi xét duyệt</td>
                @elseif($item->status === 2)
                  <td >Đã duyệt đơn hàng</td>
                @elseif($item->status === 3)
                  <td>Đang giao hàng</td>
                @elseif($item->status === 4)
                  <td >Giao hàng thành công</td>
                @endif
               <td>
                  <a href="{{ route('bill.detail',['id'=> $item->id]) }}" class="btn btn-default">Chi tiết</a>
                </td>         
              </tr>
              @endforeach
            </tbody>
          </table>
      </div>
      <div class="col-md-12">
        {{ $bills->links() }}
      </div>
    </div>
  </div>
</div>
</div>
@endsection