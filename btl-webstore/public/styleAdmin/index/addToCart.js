function addToCart(e){
	e.preventDefault();
	let urlRequest = $(this).data('url');
	let that = $(this);
	console.log(urlRequest);
	$.ajax({
	  	type:'GET',
	  	url:urlRequest,
	  	dataType: 'json',
	  	headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
	  	success: function(data){
	  		if(data.code === 200){
	  			alert("Thêm sản phẩm thành công");
	  		}
	  	},
	  	error: function(data){
	  		
	  	}
	});
	  	
}
function deleteCart(e){
	e.preventDefault();
	let urlDeleteCart = $('.delete-cart').data('url');
	let id = $(this).data('id');
	let that = $(this);
	$.ajax({
	  	type:'GET',
	  	url:urlDeleteCart,
	  	data:{id: id},
	  	success: function(data){
	  		if(data.code === 200){
	  			that.parent().parent().remove();
	  			alert('Xóa thành công sản phẩm');
	  		}
	  	},
	  	error: function(data){
	  		
	  	}
	});
	  	
}
function udpateCart(e){
	
	e.preventDefault();
	let urlDeleteCart = $('.update-cart').data('url');
	let id = $(this).data('id');
	let quantity = $(".quantity").val();
	
	let that = $(this);
	$.ajax({
	  	type:'GET',
	  	url:urlDeleteCart,
	  	data:{id: id,quantity:that.parents("tr").find(".quantity").val()},
	  	success: function(data){
	  		if(data.code === 200){
				location.reload();
	  		}
	  	},
	  	error: function(data){
	  		
	  	}
	});
	  	
}


$(function () {
	$(document).on('click','.addToCart',addToCart);
	$(document).on('click','.cart-delete',deleteCart);
	$(document).on('click','.cart-update',udpateCart);
})