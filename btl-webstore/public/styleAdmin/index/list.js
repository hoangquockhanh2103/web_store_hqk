function actionDelete(e){
	e.preventDefault();
	let urlRequest = $(this).data('url');
	let that = $(this);
	//console.log(urlRequest);
	Swal.fire({
	  	title: 'Bạn có muốn xóa hay không',
	  	html:'<p></p>',
	  	icon: 'warning',
	  	showCancelButton: true,
	  	confirmButtonColor: '#3085d6',
	  	cancelButtonColor: '#d33',
	  	confirmButtonText: 'Yes',
	}).then((result) => {
	  if (result.isConfirmed) {
	  	$.ajax({
	  		type:'GET',
	  		url:urlRequest,
	  		success: function(data){
	  			//console.log(data);
	  			if(data.code == "200"){
	  				that.parent().parent().remove();
	  				Swal.fire({
	  					title: 'Xóa thành công </br> '+ data.name,
					  	html:'<p></p>',
					  	icon: 'success',
					  	timer: 2000,
					  	timerProgressBar: true,
					  	confirmButtonColor: '#3085d6',
					  	cancelButtonColor: '#d33',
					  	confirmButtonText: 'Yes',
					  	didOpen: () => {
						  	Swal.showLoading()
						    timerInterval = setInterval(() => {
						    	const content = Swal.getHtmlContainer()
						    	if (content) {
						        	const b = content.querySelector('b')
						        	if (b) {
						          		b.textContent = Swal.getTimerLeft()
						        	}
						      	}
					    	}, 100)
						},
						willClose: () => {
					    	clearInterval(timerInterval)
						}
	  				})
	  			}
	  		},
	  		error: function(){

	  		}
	  	});
	    
	  }
	})
}

$(function () {
	$(document).on('click','.action-delete',actionDelete)
})